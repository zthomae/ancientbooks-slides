AncientBooks: The Slide Deck
=========

This is the slides I made for a talk I gave in class about what I work on. You can view the presentation [here][presentation-link].

This presentation was originally given to a group that would be familiar with the corpus and had already been introduced to topic modeling in an earlier presentation, so the explanations of these are less than thorough. However, I've expanded them somewhat, and may continue to do so.

Usage
------

- Start up a web server in the root source directory (I'm a fan of `python -m SimpleHTTPServer` myself). Then navigate to the web server in a web browser.
- The presentation is built for [deck.js][deckjs-link] (and with a stripped-down version of its source distribution). The presentationw was meant to be run in a modern web browser.
- Some slides have toggleable images. Click on the images to find them. (A tip: The toggles are global. For best effect, revert toggles back to their original state before moving on to the next slide.)
- The slides are all in index.html. This file was based on the boilerplate.html file distributed with deck.js. I've tried to make it readable by itself.

Licensing
---------

deck.js is dual-licensed under the GPL license and the MIT license.

- - -

AncientBooks is a project of the [Hazy research group][hazy-link].

[presentation-link]: http://zthomae.github.com/ancientbooks-slides/
[deckjs-link]: http://imakewebthings.com/deck.js/
[hazy-link]: http://hazy.cs.wisc.edu/hazy/